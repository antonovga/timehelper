require 'minitest/autorun'
require 'minitest/benchmark'
require 'active_support'

module TimeHelper
  SECONDS_IN_MINUTE = 60
  SECONDS_IN_HOUR = SECONDS_IN_MINUTE*60
  SECONDS_IN_DAY = SECONDS_IN_HOUR*24
  SECONDS_IN_MONTH = SECONDS_IN_DAY*30
  SECONDS_IN_YEAR = SECONDS_IN_MONTH*12


  def self.string_to_seconds(str)
    result_hash = str.scan(/\d+\s\w+/).reduce({}) do |hash, pair|
      value, key = pair.split(/\s/)
      hash[key.chomp('s')] = value
      hash
    end

    seconds = (result_hash['second']&.to_i || 0)
    seconds += (result_hash['minute']&.to_i || 0)*SECONDS_IN_MINUTE
    seconds += (result_hash['hour']&.to_i || 0)*SECONDS_IN_HOUR
    seconds += (result_hash['day']&.to_i || 0)*SECONDS_IN_DAY
    seconds += (result_hash['month']&.to_i || 0)*SECONDS_IN_MONTH
    seconds += (result_hash['year']&.to_i || 0)*SECONDS_IN_YEAR
    seconds
  end

  # в rails есть удобный ActiveSupport
  def self.string_to_seconds_as(str)
    result_hash = str.scan(/\d+\s\w+/).reduce({}) do |hash, pair|
      value, key = pair.split(/\s/)
      hash[ActiveSupport::Inflector::singularize(key)] = value
      hash
    end

    seconds = (result_hash['second']&.to_i || 0)
    seconds += (result_hash['minute']&.to_i || 0)*SECONDS_IN_MINUTE
    seconds += (result_hash['hour']&.to_i || 0)*SECONDS_IN_HOUR
    seconds += (result_hash['day']&.to_i || 0)*SECONDS_IN_DAY
    seconds += (result_hash['month']&.to_i || 0)*SECONDS_IN_MONTH
    seconds += (result_hash['year']&.to_i || 0)*SECONDS_IN_YEAR
    seconds
  end

  # Не пройдет дополнительный тест, но по заданию проходит, хотя я не любитель regexp'ов
  def self.string_to_seconds_regex(str)
    result_hash = str.match(/((?<year>\d+) year(s)?)?(\s)?((?<month>\d+) month(s)?)?(\s)?((?<day>\d+) day(s)?)?(\s)?((?<hour>\d+) hour(s)?)?(\s)?((?<minute>\d+) minute(s)?)?(\s)?((?<second>\d+) second(s)?)?(\s)?/)

    seconds = (result_hash['second']&.to_i || 0)
    seconds += (result_hash['minute']&.to_i || 0)*SECONDS_IN_MINUTE
    seconds += (result_hash['hour']&.to_i || 0)*SECONDS_IN_HOUR
    seconds += (result_hash['day']&.to_i || 0)*SECONDS_IN_DAY
    seconds += (result_hash['month']&.to_i || 0)*SECONDS_IN_MONTH
    seconds += (result_hash['year']&.to_i || 0)*SECONDS_IN_YEAR
    seconds
  end

  def self.string_to_seconds_alt(str)
    durations = { year: 31104000, month: 2592000, day: 86400, hour: 3600, minute: 60, second: 1 }
    str.scan(/(\d+) (\w+(?<!s))/).reduce(0) { |sum, pair| sum + pair[0].to_i * durations[pair[1].to_sym] }
  end

  # # Раскомментировать для теста regex версии
  # def self.string_to_seconds(str)
  #   self.string_to_seconds_regex(str)
  # end

  # # Раскомментировать для теста ActiveSupport версии
  # def self.string_to_seconds(str)
  #   self.string_to_seconds_as(str)
  # end

  # # Раскомментировать для теста alt версии
  # def self.string_to_seconds(str)
  #   self.string_to_seconds_alt(str)
  # end
end

class TestTimeHelper < Minitest::Test

  def test_seconds_only
    assert_equal TimeHelper.string_to_seconds('5 seconds'), 5
    assert_equal TimeHelper.string_to_seconds('1 second'), 1
    assert_equal TimeHelper.string_to_seconds('45 seconds'), 45
  end
  def test_minutes_only
    assert_equal TimeHelper.string_to_seconds('5 minutes'), 5*TimeHelper::SECONDS_IN_MINUTE
    assert_equal TimeHelper.string_to_seconds('1 minute'), 1*TimeHelper::SECONDS_IN_MINUTE
    assert_equal TimeHelper.string_to_seconds('45 minutes'), 45*TimeHelper::SECONDS_IN_MINUTE
  end
  def test_hours_only
    assert_equal TimeHelper.string_to_seconds('5 hours'), 5*TimeHelper::SECONDS_IN_HOUR
    assert_equal TimeHelper.string_to_seconds('1 hour'), 1*TimeHelper::SECONDS_IN_HOUR
    assert_equal TimeHelper.string_to_seconds('45 hours'), 45*TimeHelper::SECONDS_IN_HOUR
  end
  def test_days_only
    assert_equal TimeHelper.string_to_seconds('5 days'), 5*TimeHelper::SECONDS_IN_DAY
    assert_equal TimeHelper.string_to_seconds('1 day'), 1*TimeHelper::SECONDS_IN_DAY
    assert_equal TimeHelper.string_to_seconds('45 days'), 45*TimeHelper::SECONDS_IN_DAY
  end
  def test_months_only
    assert_equal TimeHelper.string_to_seconds('5 months'), 5*TimeHelper::SECONDS_IN_MONTH
    assert_equal TimeHelper.string_to_seconds('1 month'), 1*TimeHelper::SECONDS_IN_MONTH
    assert_equal TimeHelper.string_to_seconds('45 months'), 45*TimeHelper::SECONDS_IN_MONTH
  end
  def test_years_only
    assert_equal TimeHelper.string_to_seconds('5 years'), 5*TimeHelper::SECONDS_IN_YEAR
    assert_equal TimeHelper.string_to_seconds('1 year'), 1*TimeHelper::SECONDS_IN_YEAR
    assert_equal TimeHelper.string_to_seconds('45 years'), 45*TimeHelper::SECONDS_IN_YEAR
  end

  def test_different_combinations
    assert_equal TimeHelper.string_to_seconds('5 years 3 months'),
                 5*TimeHelper::SECONDS_IN_YEAR+3*TimeHelper::SECONDS_IN_MONTH
    assert_equal TimeHelper.string_to_seconds('1 hour 30 seconds'),
                 1*TimeHelper::SECONDS_IN_HOUR+30
    assert_equal TimeHelper.string_to_seconds('2 days 3 minutes 7 seconds'),
                 2*TimeHelper::SECONDS_IN_DAY+3*TimeHelper::SECONDS_IN_MINUTE+7

    assert_equal TimeHelper.string_to_seconds('1 year 2 months 3 days 2 hours 3 minutes 59 seconds'),
                 1*TimeHelper::SECONDS_IN_YEAR+2*TimeHelper::SECONDS_IN_MONTH+3*TimeHelper::SECONDS_IN_DAY+
                 2*TimeHelper::SECONDS_IN_HOUR+3*TimeHelper::SECONDS_IN_MINUTE+59
  end

  # Не работает с regex версией, но по заданию regex проходит
  def test_different_order
    assert_equal TimeHelper.string_to_seconds('3 minutes 2 days 7 seconds'),
                 2*TimeHelper::SECONDS_IN_DAY+3*TimeHelper::SECONDS_IN_MINUTE+7

    assert_equal TimeHelper.string_to_seconds('3 minutes 2 days 7 seconds'),
                 2*TimeHelper::SECONDS_IN_DAY+3*TimeHelper::SECONDS_IN_MINUTE+7

    assert_equal TimeHelper.string_to_seconds(' 7 seconds 3 minutes 2 days'),
                 2*TimeHelper::SECONDS_IN_DAY+3*TimeHelper::SECONDS_IN_MINUTE+7
  end
end